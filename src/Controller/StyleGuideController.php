<?php

namespace Drupal\style_guide_response\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Finder\Finder;


/**
 * Class StyleGuideController.
 */
class StyleGuideController extends ControllerBase {

  /**
   * Build.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return Hello string.
   */
  public function build() {

    $finder = new Finder();
    $finder
      ->files()
      ->in(DRUPAL_ROOT . '/themes')
      ->name("style-guide.html");

    foreach ($finder as $file) {
      $contents = $file->getContents();

      if ($contents != NULL) {
        return new Response($contents);
      }
      else {
        throw new NotFoundHttpException("Guide Not Found.");
      }

    }

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Style Guide not Found')
    ];
  }

}


